# tezos-kms-docker

## About 

TezosKmsDocker makes use of [ConseilKMS](https://github.com/tacoinfra/conseil-kms/) to get a remote signer up and running in one command

For more information on ConseilJS, see the [ConseilJS Documentation](https://cryptonomic.github.io/ConseilJS/#/).

`kms-conseil` is a library which provides [ConseilJS](https://github.com/cryptonomic/conseiljs) `Signer` and `KeyStore` interfaces for working with keys stored in [AWS KMS](https://aws.amazon.com/kms/). This library acts as a binding between ConseilJS and AWS KMS for working with operations in [Tezos](https://tezos.com/). 
For more information on ConseilKMS, see [ConseilKMS ReadMe](https://github.com/tacoinfra/conseil-kms/).

# configuration

In order to use keys you will need to configure a key in AWS KMS. Steps 1-12 of the [Harbinger Setup Guide](https://github.com/tacoinfra/harbinger-signer#setup-instructions) provide a brief overview of how to achieve this.

Then set the variable (in `signer.js`) : AWS_KEY_ID, AWS_REGION, NODE_ENDPOINT

Tested on node 15 and node 16

# how to run outside of docker
`npm run start`

# how to set up a baker node using an account with keys stored in aws kms

- launch necessary nodes
- launch this package, let's say it listens on KMS_ADDRESS
- when launching, take note of the `tz2...` address, this is the public key hash of your aws kms key.
- send some tezies to this address
- launch those commands :

## commands

### Reveal account
- `curl KMS_ADDRESS/reveal`

### Import account
- `tezos-client -R 'KMS_ADDRESS' import secret key USERNAME remote:tz2...`

### Test if sign operation works
- `tezos-client -l -R 'KMS_ADDRESS' sign bytes 0x1234abcd for USERNAME`

### (Optionnal) Test is sign operation works bis
- `tezos-client -l -R 'KMS_ADDRESS' transfer 1 from USERNAME to USERNAME`

### Transfer 8k tezies to this account, then check balance
- `tezos-client get balance for USERNAME`

### Register this account as delegate
- `tezos-client -l -R 'KMS_ADDRESS' register key USERNAME as delegate`

----- 

# todo
- [ ] load test 
- [ ] sec audit


# resources
https://tezos.gitlab.io/user/key-management.html
https://tezos.gitlab.io/introduction/howtorun.html#baker
https://tezos.gitlab.io/shell/cli-commands.html

https://cryptonomic.github.io/ConseilJS/#/
https://github.com/tacoinfra/conseil-kms
https://github.com/tacoinfra/conseil-kms/blob/master/src/kms-key-store.ts
https://github.com/tacoinfra/conseil-kms/blob/master/src/kms-signer.ts

https://github.com/tacoinfra/tezos-kms
https://github.com/tacoinfra/tezos-kms/blob/master/src/tezos-kms-client.ts

https://tezos.stackexchange.com/questions/1060/import-private-key
https://tezos.stackexchange.com/questions/2825/how-to-write-my-own-signer  (good)
https://tezos.stackexchange.com/questions/591/how-to-check-if-remote-signer-is-working-as-expected

https://www.google.com/search?q=tezos+bake+with+aws+kms
https://assets.tqtezos.com/docs/run-a-node/5-tezos-nodes/
https://tezos.stackexchange.com/questions/695/is-it-possible-to-run-a-baker-in-aws-and-secure-your-keys-with-aws-cloudhsm

https://docs.aws.amazon.com/kms/latest/APIReference/API_GenerateDataKeyPair.html

https://github.com/tacoinfra/remote-signer/blob/master/src/remote_signer.py
https://pytezos.baking-bad.org/tutorials/01.html#base58-encodingo

https://www.ocamlpro.com/2018/11/21/an-introduction-to-tezos-rpcs-signing-operations/ (good)

https://tezos.stackexchange.com/questions/2825/how-to-write-my-own-signer
https://github.com/lattejed/tezos-azure-hsm-signer/blob/master/server.js
https://tezos.stackexchange.com/questions/617/error-while-signing-transaction-from-remote-signer-after-updating-mainnet-sh-scr
https://gitlab.com/tezos/tezos/-/issues/482
https://tezos.stackexchange.com/questions/3277/the-signer-produced-an-invalid-signature

# to monitor 
- https://github.com/ecadlabs/signatory#tezos-address-types (good)
- https://gitlab.com/unit410/tezos-hsm-signer

# spsig
https://tezostaquito.io/docs/version/#prefixsig

# pass faucet file to running container
https://tezos.gitlab.io/introduction/howtouse.html#get-free-tez
- `tezos-client client activate account faucet_account_1 with "container:tz1YRiJE6RvoAzF4Y2c4ZzsrvucD8szuZL7K.json"`

