// https://cryptonomic.github.io/ConseilJS/#/?id=use-with-nodejs
import fetch from 'node-fetch';
import loglevel from 'loglevel';

const {getLogger} = loglevel;
const logger = getLogger("conseiljs");
logger.setLevel('debug', false); // to see only errors, set to 'error'

import pkg from 'conseil-kms-bis';

const {KmsKeyStore, KmsSigner} = pkg;
import {registerFetch, registerLogger, TezosNodeWriter} from 'conseiljs';

registerLogger(logger);
registerFetch(fetch);

const AWS_KEY_ID = process.env.AWS_KEY_ID || 'X';
const AWS_REGION = process.env.AWS_REGION || 'eu-west-1';
const NODE_ENDPOINT = process.env.NODE_ENDPOINT || 'https://granadanet.smartpy.io/';

const signer = new KmsSigner(AWS_KEY_ID, AWS_REGION);
export const keystore = await KmsKeyStore.from(AWS_KEY_ID, AWS_REGION)
const publicKeyHash = keystore.publicKeyHash

console.log({keystore})
console.log({publicKeyHash})

export default signer

export async function revealAccount() {
    const result = await TezosNodeWriter.sendKeyRevealOperation(NODE_ENDPOINT, signer, keystore);
    console.log({result})
    console.log(`Injected operation group id ${result.operationGroupID}`);
}

export async function signOperation(bytes) {
    const result = await signer.signOperation(bytes)
    return result
}

export async function signOperationBase58(bytes) {
    const result = await signer.signOperationBase58(bytes)
    return result
}
