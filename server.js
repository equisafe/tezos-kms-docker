'use strict';

import express from 'express'
import signer from './signer.js'
import {
    keystore,
    revealAccount,
    signOperationBase58,
} from "./signer.js";

const PORT = process.env.PORT || 8075;
const HOST = process.env.HOST || '0.0.0.0';

const app = express();
app.use(express.json({strict: false}))

app.get('/signer', (req, res) => {
    res.json({signer: signer})
})
app.get('/reveal', (req, res) => {
    revealAccount()
})

// https://tezos.stackexchange.com/questions/2825/how-to-write-my-own-signer
app.get('/authorized_keys', (req, res) => {
    res.json({})
})

app.get('/keys/:pkh', (req, res) => {
    const pkh = req.params.pkh
    console.log(`finding ${pkh}`)
    // always returning the aws kms public key
    res.json({
        public_key: keystore.publicKey
    })
})

app.post('/keys/:pk', async (req, res) => {
    // Sign a piece of data with a given remote key
    // receives: a string of bytes

    const body = req.body
    const bytes = Buffer.from(body, 'hex')
    const signedBase58 = await signOperationBase58(bytes)

    res.json({
        signature: signedBase58
    })
})

app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

